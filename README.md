# Introduction

Email archiving provides various benefits for your company. Piler is a robust open source email archiving system that may be used instead of a commercial solution. This guide will lead you through the steps necessary to install Piler on Ubuntu 20.04, assuming you already have a machine running Ubuntu 20.04.

# Setup FQDN Hostname 

```
hostnamectl set-hostname --static piler
```

```
nano /etc/hosts
```

| /etc/hosts |
|---|
| 127.0.0.1 localhost<br>1.2.3.5 piler.yourdomain.com piler |

**Note:** Change the hostname and IP address of your piler server to match the parameters in your environment.

## Install Dependancies

Install the Piler requirements listed below.

```
apt update
```

```
apt install sysstat build-essential libwrap0-dev libpst-dev tnef libytnef0-dev unrtf catdoc libtre-dev tre-agrep poppler-utils libzip-dev unixodbc libpq5 software-properties-common libpoppler-dev openssl libssl-dev python3-mysqldb memcached pwgen telnet
```

## Install MariaDB 10.04

```
apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
add-apt-repository 'deb [arch=amd64] https://mirror.netcologne.de/mariadb/repo/10.4/ubuntu focal main'
apt install mariadb-{server,client} libmariadb-client-lgpl-dev-compat
```

## Configure MariaDB

Create the configuragtion file:

```
nano /etc/mysql/conf.d/mailpiler.conf 
```

| /etc/mysql/conf.d/mailpiler.conf |
|----------------------------------|
|innodb_buffer_pool_size=256M<br>innodb_flush_log_at_trx_commit=1<br>innodb_log_buffer_size=64M<br>innodb_log_file_size=16M<br>query_cache_size=0<br>query_cache_type=0<br>query_cache_limit=2M |

**Note:** The settings are completely arbitrary. You may need to modify them based on your environment. To learn more about these settings, go to the relevant [MariaDB documentation](https://mariadb.com/kb/en/innodb-buffer-pool/).

## Install Sphinx Search

Sphinx is an open source full text search server that prioritizes efficiency, relevance (search quality), and integration ease.

```
mkdir -p /opt/mailpiler/sphinxsearch/
mkdir -p /opt/mailpiler/sphinxsearch/
wget http://sphinxsearch.com/files/sphinx-3.3.1-b72d67b-linux-amd64.tar.gz
tar xfz sphinx-*-linux-amd64.tar.gz
cp -v sphinx-*/bin/* /usr/local/bin/
```


